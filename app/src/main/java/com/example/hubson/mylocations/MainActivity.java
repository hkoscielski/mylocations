package com.example.hubson.mylocations;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hubson.mylocations.listeners.OnLocationListChangedListener;
import com.example.hubson.mylocations.listeners.OnStartDragListener;
import com.example.hubson.mylocations.models.MyLocation;
import com.example.hubson.mylocations.ui.LocationAdapter;
import com.example.hubson.mylocations.ui.SimpleItemTouchHelperCallback;
import com.example.hubson.mylocations.utils.GPS;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnLocationListChangedListener, OnStartDragListener {

    private RecyclerView recyclerView;
    private LocationAdapter adapter;
    private List<MyLocation> locations;
    private ItemTouchHelper itemTouchHelper;

    private static final String PREFERENCES_FILE = "my_locations_preferences";
    private static final String LOCATION_LIST = "location_list";
    private static final String EMPTY_LOCATION_LIST = "empty_location_list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setUpRecyclerView();
        setUpFabAdd();
    }

    private List<MyLocation> getLocationList() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        String jsonLocationList = preferences.getString(LOCATION_LIST, EMPTY_LOCATION_LIST);

        if (jsonLocationList.equals(EMPTY_LOCATION_LIST)) return new ArrayList<>();
        return new Gson().fromJson(jsonLocationList, new TypeToken<List<MyLocation>>() {
                }.getType());

    }

    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.location_list);
        recyclerView.setHasFixedSize(true);
        locations = getLocationList();
        adapter = new LocationAdapter(this, locations, this, this);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);
    }

    private void setUpFabAdd() {
        FloatingActionButton fabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                final View mView = getLayoutInflater().inflate(R.layout.dialog_add_location, null);

                builder.setView(mView);

                final Button addBtn = (Button) mView.findViewById(R.id.addBtn);
                final AlertDialog dialog = builder.create();
                final ImageButton locationBtn = (ImageButton) mView.findViewById(R.id.searchBtn);
                final TextView addressTv = (TextView) mView.findViewById(R.id.addressTv);
                final EditText descriptionEv = (EditText) mView.findViewById(R.id.descriptionEv);

                addBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GPS gps = new GPS(MainActivity.this);
                        MyLocation location = new MyLocation(gps.getLatitude(), gps.getLongitude(),
                                addressTv.getText().toString(), descriptionEv.getText().toString());
                        locations.add(location);
                        onNoteListChanged(locations);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });

                locationBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {//
                        GPS gps = new GPS(MainActivity.this);
                        showToast(String.format("Your current location\nLongitude: %s\nLatitude: %s", gps.getLongitude(), gps.getLatitude()));
                        String address = gps.getAddress();
                        if(address.equals(""))
                            showToast("Unable to load current address. Check if your GPS and network are enable");
                        else
                            addressTv.setText(address);
                        gps.stopGPS();
                    }


                });

                dialog.show();

            }
        });
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onNoteListChanged(List<MyLocation> locations) {
        saveLocations(locations);
    }

    private void saveLocations(List<MyLocation> locations) {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Gson gson = new Gson();
        String jsonLocationList = gson.toJson(locations);

        editor.putString(LOCATION_LIST, jsonLocationList);
        editor.apply();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
