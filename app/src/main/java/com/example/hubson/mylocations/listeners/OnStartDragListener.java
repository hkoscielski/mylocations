package com.example.hubson.mylocations.listeners;

import android.support.v7.widget.RecyclerView;

public interface OnStartDragListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
