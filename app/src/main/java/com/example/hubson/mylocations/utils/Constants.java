package com.example.hubson.mylocations.utils;

public class Constants {
    public static final String LONGITUDE = "LONGITUDE";
    public static final String LATITUDE = "LATITUDE";
    public static final String ADDRESS = "ADDRESS";

    public static final double DEFAULT_LONGITUDE = 0;
    public static final double DEFAULT_LATITUDE = 0;
}
