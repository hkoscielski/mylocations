package com.example.hubson.mylocations.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

public class PermissionUtils {

    private static final int REQUEST_CODE = 1;

    public static boolean isLocationPermissionGranted(Activity activity) {
        return isPermissionGranted(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                || isPermissionGranted(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    public static void requestLocationPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
    }

    private static boolean isPermissionGranted(Activity activity, String permission) {
        return ActivityCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED;
    }
}
