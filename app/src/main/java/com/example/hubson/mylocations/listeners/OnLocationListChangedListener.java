package com.example.hubson.mylocations.listeners;

import com.example.hubson.mylocations.models.MyLocation;

import java.util.List;

public interface OnLocationListChangedListener {
    void onNoteListChanged(List<MyLocation> locations);
}
