package com.example.hubson.mylocations.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hubson.mylocations.MapsActivity;
import com.example.hubson.mylocations.R;
import com.example.hubson.mylocations.listeners.OnLocationListChangedListener;
import com.example.hubson.mylocations.listeners.OnStartDragListener;
import com.example.hubson.mylocations.models.MyLocation;

import java.util.Collections;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder>
                            implements ItemTouchHelperAdapter {
    private Context context;
    private List<MyLocation> locations;
    private OnStartDragListener dragListener;
    private OnLocationListChangedListener listChangedListener;

    public LocationAdapter(Context context, List<MyLocation> locations,
                           OnStartDragListener dragListener, OnLocationListChangedListener listChangedListener) {
        this.context = context;
        this.locations = locations;
        this.dragListener = dragListener;
        this.listChangedListener = listChangedListener;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item, parent, false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LocationViewHolder holder, int position) {
        final MyLocation location = locations.get(position);

        holder.detailLayout.setVisibility(View.GONE);
        holder.mName.setText(location.getAddress());
        holder.mDescription.setText(location.getDescription());

        holder.mHandleImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    dragListener.onStartDrag(holder);
                }
                return false;
            }
        });

        holder.mMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MapsActivity.class);
                intent.putExtra("LATITUDE", location.getLatitude());
                intent.putExtra("LONGITUDE", location.getLongitude());
                intent.putExtra("ADDRESS", location.getAddress());
                context.startActivity(intent);
            }
        });

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.detailLayout.isShown()) {
                    holder.detailLayout.setVisibility(View.GONE);
                } else {
                    holder.detailLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(locations, fromPosition, toPosition);
        listChangedListener.onNoteListChanged(locations);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        locations.remove(position);
        listChangedListener.onNoteListChanged(locations);
        notifyItemRemoved(position);
    }

    static class LocationViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        final View mView;
        final TextView mName;
        final ImageView mHandleImage;
        final TextView mDescription;
        final Button mMapBtn;
        final LinearLayout mainLayout;
        final LinearLayout detailLayout;
        final CardView cardView;

        LocationViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mName = (TextView) itemView.findViewById(R.id.location_nameTv);
            mHandleImage = (ImageView) itemView.findViewById(R.id.handleImage);
            mDescription = (TextView) itemView.findViewById(R.id.descriptionTv);
            mMapBtn = (Button) itemView.findViewById(R.id.mapBtn);
            mainLayout = (LinearLayout) itemView.findViewById(R.id.mainPartLayout);
            detailLayout = (LinearLayout) itemView.findViewById(R.id.detailsLayout);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(Color.parseColor("#FEFEFE"));
        }
    }
}
