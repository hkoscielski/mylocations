package com.example.hubson.mylocations;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.example.hubson.mylocations.utils.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int ZOOM = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setFullscreen();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        double longitude = getIntent().getDoubleExtra(Constants.LONGITUDE, Constants.DEFAULT_LONGITUDE);
        double latitude = getIntent().getDoubleExtra(Constants.LATITUDE, Constants.DEFAULT_LATITUDE);
        String address = getIntent().getStringExtra(Constants.ADDRESS);

        LatLng position = new LatLng(latitude, longitude);
        googleMap.addMarker(new MarkerOptions().position(position).title(address));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, ZOOM));
    }

    private void setFullscreen() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}
