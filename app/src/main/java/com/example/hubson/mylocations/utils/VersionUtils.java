package com.example.hubson.mylocations.utils;

import android.os.Build;

public class VersionUtils {

    public static boolean hasEnoughVersionSDK(int minVersion) {
        return minVersion <= Build.VERSION.SDK_INT;
    }
}
