package com.example.hubson.mylocations.models;

public class MyLocation {

    private double longitude;
    private double latitude;
    private String address;
    private String description;

    public MyLocation(double latitude, double longitude, String address, String description) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.description = description;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }
}
