package com.example.hubson.mylocations.ui;

public interface ItemTouchHelperViewHolder {
    void onItemSelected();
    void onItemClear();
}
